require("../dbConfig");

const programModel = require("../models/program.model");

const filters = {

  show: function(req, res) {

    programModel.find({}, { _id: 0 }, function(err, data) {
      if (err) throw err;
      else {
        res.send(data);
      }
    }).limit(parseInt(req.params.limit)).skip(parseInt(req.params.skip))
  },

  filters: function(req, res) {

    let query = {
      maxFee: { $lte: req.body.fee },
      maxDuration: { $lte: req.body.duration },

    };
    Object.keys(query).forEach(key => query[key] === undefined && delete query[key]);

    programModel.find(query, {}, function(err, data) {
      if (err) throw err;
      else {
        res.send(data);
      }
    }).limit(parseInt(req.params.limit)).skip(parseInt(req.params.skip));
  },
};
module.exports = filters
