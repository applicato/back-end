const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const Schema = mongoose.Schema;

const programSchema = new Schema({
    "title": String,
    "name": String,
    "pName": String,
    "field": String,
    "university": String,
    "pUniversity": String,
    "fullDuration": Number,
    "partDuration": Number,
    "language": String,
    "pLanguage": String,
    "degree": String,
    "pDegree": String,
    "fullOrPart": String,
    "description": String,
    "pDescription": String,
    "link": String,
    "ielts": Number,
    "ieltsRes": Number,
    "toefl": Number,
    "toeflRes": Array,
    "duolingo": Number,
    "duolingoRes": String,
    "fee": Number,
    "currency": String
});

programSchema.plugin(mongoosePaginate);
let program = mongoose.model("programs", programSchema);
module.exports = program;
